%
% Sensor fusion with Kalman Filter 
% for linear process of free fall in vacuum
% 

clear all
close all

% gravitational constant
g = -9.8;

% simulation time step
dT = 1;

% simulation max time
TMax = 40;

% === SENSORS ====================
% only position is measured

% sensors' noise covariance matrix
r = [200 150 100];
R = diag(r .* r)

% sensors' bias
Bias = [10 -10 0]

% === PROCESS ====================
% process model
A = [1 dT; 0 1];
B = [0.5 * dT^2; dT];
U = [g];
C = [1 0; 1 0; 1 0];

% process noise covariance matrix
q = [0 0]';
Q = q * q'

% initial state
X = [0; 0]; 

% initial state uncertainty
p = [10 5]';
P = p * p'

% ================================
T = 0:dT:TMax;
N = length(T);
nSensor = length(r);

Yproc = [];
Ymeas = [];
Ykalm = [];
Kkalm = [];

for t=T
    Y = 0.5 * g * t ^ 2;

    Ym = zeros(nSensor, 1);
    for i=1:nSensor
        Ym(i) = Y + normrnd(Bias(i), r(i));
    endfor

    Yproc = [Yproc Y];
    Ymeas = [Ymeas Ym];
    Xhat = A * X + B * U;
    Yhat = C * Xhat;
    [X, P, K] = kalman(A, C, Xhat, Yhat, Ym, P, Q, R);
    Ykalm = [Ykalm X(1)];
    Kkalm = [Kkalm K];
endfor

figure()
hold all 
title("Results")
plot(T, Yproc, "-g")
str = { "process" };
for i=1:length(r)
    plot(T, Ymeas(i, :), 'x')
    str = [str , strcat('sensor ' , num2str(i))];
endfor
plot(T, Ykalm, 'o')
str = [str , "kalman"];
legend(str{:})

figure()
hold all
title("Kalman Gain")
str = { };
for i=1:size(Kkalm, 1)
    for k=1:nSensor
        plot(T, Kkalm(i, k:nSensor:end))
        str = [str , strcat('var ' , num2str(i), ' component ', num2str(k))];
    endfor
endfor
legend(str{:})

MSEkalm = sum((Ykalm - Yproc).^2) / length(T);
    printf('MSE KF = %.1f\n', MSEkalm);
for i=1:nSensor
    MSEmeas = sum((Ymeas(i, :) - Yproc).^2) / length(T);
    printf('MSE sensor %d = %.1f\n', i, MSEmeas);
endfor
