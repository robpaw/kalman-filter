%
% Simple Kalman Filter for pendulum dynamics
% assuming small thetas (e.i. sin(theta) ~ theta) 
% 

clear all
close all

% gravitational constant
g = 9.8;

% string length
l = 2;

% initial theta
theta0 = 90 * pi / 180.0

% simulation time step
dT = 0.01;

% simulation max time
TMax = 10;

% === SENSORS ====================
% only position is measured

% sensors' noise covariance matrix
r = [0.2 0.2];
R = diag(r .* r)

% sensors' bias
Bias = [0 0]

% === PROCESS ====================
% process model
% assumption: sin(theta) ~ theta
% state: [theta velocity acceleration]'
A = [1 dT 0.5 * dT ^ 2; 0 1 dT; -g / l 0 0];
B = [0; 0; 0];
U = [0];
C = repmat([l 0 0], length(r), 1);

% process noise covariance matrix
q = [0.01 0.01 0.01]';
Q = diag(q .* q)

% initial state
X = [theta0; 0; -g / l * theta0]; 

% initial state uncertainty
P = Q

% ================================
T = 0:dT:TMax;
N = length(T);
nSensor = length(r);

Yproc = sin(pendulum(T, theta0)) * l;
Ymeas = [];
for i=1:nSensor
    err = normrnd(Bias(i), r(i), 1, N);
    Ymeas = [Ymeas; Yproc + err];
endfor

Ykalm = [];
Kkalm = [];

for i=1:N
    Xhat = A * X + B * U;
    Yhat = C * Xhat;
    [X, P, K] = kalman(A, C, Xhat, Yhat, Ymeas(:, i), P, Q, R);
    Ykalm = [Ykalm X(1) * l];
    Kkalm = [Kkalm K];
endfor

figure()
hold all 
title("Results")
plot(T, Yproc, "-g")
str = { "process" };
for i=1:length(r)
    plot(T, Ymeas(i, :), 'x')
    str = [str , strcat('sensor ' , num2str(i))];
endfor
plot(T, Ykalm, '-')
str = [str , "kalman"];
legend(str{:})

figure()
hold all 
title("Results")
plot(T, Yproc, "-g")
str = { "process" };
plot(T, Ykalm, '-')
str = [str , "kalman"];
legend(str{:})


figure()
hold all
title("Kalman Gain")
str = { };
for i=1:size(Kkalm, 1)
    for k=1:nSensor
        plot(T, Kkalm(i, k:nSensor:end))
        str = [str , strcat('var ' , num2str(i), ' component ', num2str(k))];
    endfor
endfor
legend(str{:})

MSEkalm = sum((Ykalm - Yproc).^2) / length(T);
    printf('MSE KF = %.5f\n', MSEkalm);
for i=1:nSensor
    MSEmeas = sum((Ymeas(i, :) - Yproc).^2) / length(T);
    printf('MSE sensor %d = %.5f\n', i, MSEmeas);
endfor
