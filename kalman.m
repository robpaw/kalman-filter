function [X, P, K] = kalman(F, H, Xhat, Yhat, Ymeas, P, Q, R)
        Phat = F * P * F' + Q;
        
        K = Phat * H' * inv(H * Phat * H' + R);
        D = Ymeas - Yhat;
        X = Xhat + K * D;
        P = (eye(length(diag(P))) - K * H) * Phat;
endfunction
