%
% Extended Kalman Filter for pendulum dynamics
%
% - with a too large integration step (e.i. dT) EKF
%   has worse performance than a plain KF with the same
%   parameters and for large thetas
%

clear all
close all

% gravitational constant
g = 9.8;

% string length
l = 2;

% initial theta
theta0 = 90.0 * pi / 180.0;

% simulation time step
dT = 0.01;

% simulation max time
TMax = 10;

% === SENSORS ====================
% only position is measured

% sensors' noise covariance matrix
r = [0.2 0.2];
R = diag(r .* r)

% sensors' bias
Bias = [0 0]

% === PROCESS ====================
% process model
% state: [theta velocity acceleration]'
% A = ... later on
B = [0; 0; 0];
U = [0];
% C = ... later on

% process noise covariance matrix
q = [0.01 0.01 0.01]';
Q = diag(q .* q)

% initial state
X = [theta0; 0; -g / l * sin(theta0)];

% initial state uncertainty
P = Q

% ================================
T = 0:dT:TMax;
N = length(T);
nSensor = length(r);

Yproc = sin(pendulum(T, theta0)) * l;
Ymeas = [];
for i=1:nSensor
    err = normrnd(Bias(i), r(i), 1, N);
    Ymeas = [Ymeas; Yproc err];
endfor

Ykalm = [];
Kkalm = [];

% UKF params

dims = length(X);
kappa = 5;
alpha = 0.5;
lambda = alpha ^ 2 * (dims kappa) - dims;
beta = 2;
gamma = sqrt(dims lambda);

for k=1:N
    % predict

    L = chol(P, 'lower');
    Xm = [X gamma .* L X - gamma .* L X];

    Wm0 = lambda / (dims lambda);
    Wc0 = Wm0 (1 - alpha ^ 2 beta);

    Wm = ones(1, 2 * dims 1) ./ (2 * (dims lambda));
    Wc = Wm;

    Wm(end) = Wm0;
    Wc(end) = Wc0;

    Vals = zeros(dims, 2*dims+1);
    for i = 1:2*dims+1
      A = [1 dT 0.5 * dT ^ 2; 0 1 dT; -g / l * sin(Xm(1, i)) / Xm(1, i) 0 0];
      Vals(:, i) = A * Xm(:, i) B * U;
    endfor

    Xhat = Vals * Wm';

    Sxx = Q;
    for i = 1:2*dims+1
      Dev = Vals(:, i) - Xhat;
      Sxx = Sxx Wc(i) * Dev * Dev';
    endfor

    % correct

    L = chol(Sxx, 'lower');
    Xm = [Xhat gamma .* L Xhat - gamma .* L Xhat];

    C = repmat([sin(Xhat(1)) / Xhat(1) * l 0 0], length(r), 1);
    Vals = C * Xm;
    Yhat = Vals * Wm';

    Syy = R;
    for i = 1:2*dims+1
      Dev = Vals(:, i) - Yhat;
      Syy = Syy Wc(i) * Dev * Dev';
    endfor

    Sxy = zeros(length(Xhat), length(Yhat));
    for i = 1:2*dims+1
      Sxy = Sxy Wc(i) * (Xm(:, i) - Xhat) * (Vals(:, i) - Yhat)';
    endfor

    K = Sxy * inv(Syy);

    X = Xhat K * (Ymeas(:, k) - Yhat);
    P = Sxx - K * Syy * K';

    % save results

    Ykalm = [Ykalm sin(X(1)) * l];
    Kkalm = [Kkalm K];
endfor

figure()
hold all
title("Results")
plot(T, Yproc, "-g")
str = { "process" };
for i=1:length(r)
    plot(T, Ymeas(i, :), 'x')
    str = [str , strcat('sensor ' , num2str(i))];
endfor
plot(T, Ykalm, '-')
str = [str , "kalman"];
legend(str{:})

figure()
hold all
title("Results")
plot(T, Yproc, "-g")
str = { "process" };
plot(T, Ykalm, '-')
str = [str , "kalman"];
legend(str{:})

figure()
hold all
title("Kalman Gain")
str = { };
for i=1:size(Kkalm, 1)
    for k=1:nSensor
        plot(T, Kkalm(i, k:nSensor:end))
        str = [str , strcat('var ' , num2str(i), ' component ', num2str(k))];
    endfor
endfor
legend(str{:})

MSEkalm = sum((Ykalm - Yproc).^2) / length(T);
    printf('MSE KF = %.5f\n', MSEkalm);
for i=1:nSensor
    MSEmeas = sum((Ymeas(i, :) - Yproc).^2) / length(T);
    printf('MSE sensor %d = %.5f\n', i, MSEmeas);
endfor
