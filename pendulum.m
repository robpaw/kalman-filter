
function theta = pendulum(t, theta0)
    [z, t] = lsode(@equation, [theta0, 0], t);
    theta = z(:, 1)';
end

function dzdt = equation(z, t)
    g = 9.8;
    l = 2;
    z1 = z(1);
    z2 = z(2);
    dzdt = [z2; -g / l * sin(z1)];
end
